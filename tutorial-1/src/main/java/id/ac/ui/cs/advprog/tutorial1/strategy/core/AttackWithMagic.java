package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithMagic implements AttackBehavior {
	@Override
	public String getType() {
		return "magic";
	}

	@Override
	public String attack() {
		return "Attack with magic";
	}
}
