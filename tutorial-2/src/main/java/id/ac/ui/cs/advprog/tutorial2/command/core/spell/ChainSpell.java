package id.ac.ui.cs.advprog.tutorial2.command.core.spell;


import java.util.ArrayList;
import java.util.Stack;


public class ChainSpell implements Spell {
    // TODO: Complete Me
    ArrayList<Spell> spells;
    public ChainSpell(ArrayList<Spell> spells){
        this.spells = spells;
    }

    public void cast() {
        for(Spell spell : spells){
            spell.cast();
        }
    }

    @Override
    public void undo() {
        for(int index = spells.size() - 1; index >= 0; index--){
            spells.get(index).cast();
        }

    }

    @Override
    public String spellName() {
        return "ChainSpell";
    }
}
