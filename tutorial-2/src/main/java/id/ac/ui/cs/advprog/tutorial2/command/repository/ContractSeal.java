package id.ac.ui.cs.advprog.tutorial2.command.repository;

import id.ac.ui.cs.advprog.tutorial2.command.core.spell.BlankSpell;
import id.ac.ui.cs.advprog.tutorial2.command.core.spell.Spell;
import org.springframework.stereotype.Repository;

import java.util.*;

@Repository
public class ContractSeal {
    private Spell latestSpell;
    private Map<String, Spell> spells;

    public ContractSeal() {
        spells = new HashMap<>();
    }

    public void registerSpell(Spell spell) {
        spells.put(spell.spellName(), spell);
    }

    public void castSpell(String spellName) {
        Spell temp = spells.get(spellName);
        latestSpell = temp;
        latestSpell.cast();
    }

    public void undoSpell() {
        if(latestSpell != null) {
            latestSpell.undo();
            latestSpell = null;
        }
    }


    public Collection<Spell> getSpells() { return spells.values(); }
}
