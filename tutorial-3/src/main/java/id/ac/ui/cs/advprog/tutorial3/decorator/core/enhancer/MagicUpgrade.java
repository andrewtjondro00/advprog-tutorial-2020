package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Weapon;

import java.util.Random;

public class MagicUpgrade extends Weapon {
    int upgradeValue;
    Weapon weapon;

    public MagicUpgrade(Weapon weapon) {

        this.weapon= weapon;
        Random rand = new Random();
        upgradeValue = 15 + rand.nextInt(5);
    }

    @Override
    public String getName() {
        return weapon.getName();
    }

    // Senjata bisa dienhance hingga 15-20 ++
    @Override
    public int getWeaponValue() {
        return upgradeValue + weapon.getWeaponValue();
    }

    @Override
    public String getDescription() {
        return weapon.getDescription();
    }
}
