package id.ac.ui.cs.advprog.tutorial3.composite.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PremiumMemberTest {
    private Member member;

    @BeforeEach
    public void setUp() {
        member = new PremiumMember("Wati", "Gold Merchant");
    }

    @Test
    public void testMethodGetName() {
        assertEquals("Wati", member.getName());
    }

    @Test
    public void testMethodGetRole() {
        assertEquals("Gold Merchant", member.getRole());
    }

    @Test
    public void testMethodAddChildMember() {
        Member dummy = new OrdinaryMember("coba", "coba");
        member.addChildMember(dummy);
        assertEquals(1, member.getChildMembers().size());
        assertEquals(dummy.getName(), member.getChildMembers().get(0).getName());
        assertEquals(dummy.getRole(), member.getChildMembers().get(0).getRole());

        Member dummy2 = new PremiumMember("coba", "coba 2");
        member.addChildMember(dummy2);
        assertEquals(2, member.getChildMembers().size());
        assertEquals(dummy2.getName(), member.getChildMembers().get(1).getName());
        assertEquals(dummy2.getRole(), member.getChildMembers().get(1).getRole());
    }

    @Test
    public void testMethodRemoveChildMember() {
        Member dummy = new OrdinaryMember("coba", "coba");
        member.addChildMember(dummy);
        assertEquals(1, member.getChildMembers().size());

        member.removeChildMember(dummy);
        assertEquals(0, member.getChildMembers().size());
    }

    @Test
    public void testNonGuildMasterCanNotAddChildMembersMoreThanThree() {
        Member dummy = new OrdinaryMember("coba", "coba");
        for(int i = 0; i < 10; i++) member.addChildMember(dummy);
        assertEquals(3, member.getChildMembers().size());
    }

    @Test
    public void testGuildMasterCanAddChildMembersMoreThanThree() {
        Member guildMaster = new PremiumMember("Heathcliff", "Master");
        Member dummy = new OrdinaryMember("coba", "coba");
        for(int i = 0; i < 10; i++) guildMaster.addChildMember(dummy);
        assertEquals(10, guildMaster.getChildMembers().size());
    }
}
