package id.ac.ui.cs.tutorial0.service;

public interface AdventurerCalculatorService {
    public String powerClassifier(int power);
    public int countPowerPotensialFromBirthYear(int birthYear);
}
